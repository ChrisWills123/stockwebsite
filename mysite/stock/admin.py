from django.contrib import admin

from stock.models import Category, Item
# Register your models here.
class ItemInline(admin.StackedInline):
    model = Item
    extra = 3

class CategoryAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['name']})
    ]
    inlines = [ItemInline]

admin.site.register(Category)