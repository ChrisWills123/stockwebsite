from django.conf.urls import patterns, url
from stock import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^(?P<category_id>\d+)/add$', views.detail, name='detail'),
    url(r'^(?P<category_id>\d+)/remove$', views.remove, name='remove'),
    url(r'^(?P<category_id>\d+)/$', views.contents, name='contents'),
    url(r'^(?P<category_id>\d+)/addItem/$', views.addItem, name='addItem'),
    url(r'^(?P<category_id>\d+)/removeItem/$', views.removeItem, name='removeItem'),
)