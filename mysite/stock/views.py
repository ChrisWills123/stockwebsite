from django.core.urlresolvers import reverse
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404

from stock.models import Category, Item
# Create your views here.
def index(request):
    category_list = Category.objects.all()
    context = {'category_list': category_list}
    return render(request, 'categories/index.html', context)

def detail(request, category_id):
    category = get_object_or_404(Category, pk=category_id)
    return render(request, 'categories/detail.html', {'category': category})

def contents(request, category_id):
    category = get_object_or_404(Category, pk=category_id)
    return render(request, 'categories/contents.html', {'category': category})

def addItem(request, category_id):
    c = get_object_or_404(Category, pk=category_id)
    
    obj, created = Item.objects.get_or_create(name=request.POST['item'], category_id=category_id)

    obj.quantity += int(request.POST['numberOfItems'])
    obj.save()
    return HttpResponseRedirect(reverse('stock:contents', args=(c.id,)))

def remove(request, category_id):
    category = get_object_or_404(Category, pk=category_id)
    return render(request, 'categories/remove.html', {'category': category})

def removeItem(request, category_id):
    c = get_object_or_404(Category, pk=category_id)
    
    obj = Item.objects.get(name=request.POST['item'], category_id=category_id)

    obj.quantity -= int(request.POST['numberOfItems'])
    obj.save()
    return HttpResponseRedirect(reverse('stock:contents', args=(c.id,)))    